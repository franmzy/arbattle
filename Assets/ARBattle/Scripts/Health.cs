﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour
{
	public const int maxHealth = 100;
	public int currentHealth = maxHealth;
	public RectTransform healthBar;

	private GameObject _uiManager;
	private UIManager uiManagerComponent;

	void Start ()
	{
		_uiManager = GameObject.Find ("UIManager");
		uiManagerComponent = _uiManager.GetComponent<UIManager> ();
	}

	public void TakeDamage (int amount)
	{
		currentHealth -= amount;
		if (currentHealth <= 0) {
			currentHealth = 0;
			Debug.Log ("Dead!");
		}

		healthBar.sizeDelta = new Vector2 (currentHealth, healthBar.sizeDelta.y);

		uiManagerComponent.Flash ("red");
	}
}