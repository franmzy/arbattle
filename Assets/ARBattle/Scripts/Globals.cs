﻿public class Globals
{
	public enum TargetID
	{
		backgroundTarget1 = 0,
		backgroundTarget2 = 1,
		userTarget1 = 2,
		userTarget2 = 3,
		shiedlTarget1 = 4,
		shiedlTarget2 = 5
	}

	public struct Epsilon
	{
		public static float initilizeTime = 0.5f;
	}
}
