﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	private Image _redImage;

	public float _speed = 0.005f;

	void Start ()
	{
		_redImage = GameObject.Find ("Red Image").GetComponent<Image> ();
	}

	public void Flash (string color)
	{
		if (color == "red") {
			Invoke ("FadeIn", 0);
		}
	}

	private void FadeIn ()
	{
		Color color = _redImage.color;
		color.a = color.a + 0.2f;
		_redImage.color = color;

		if (color.a >= 1) {
			Invoke ("FadeOut", _speed);
		}
		else {
			Invoke ("FadeIn", _speed);
		}
	}

	private void FadeOut ()
	{
		Color color = _redImage.color;
		color.a = color.a - 0.2f;
		_redImage.color = color;

		if (_redImage.color.a > 0) {
			Invoke ("FadeOut", _speed);
		}
	}
}
