﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

//using UnityEditor;

public class LocationBehaviour : NetworkBehaviour
{
	#region PUBLIC_MEMBER_VARIABLES

	public GameObject _backgroundPrefab;
	public GameObject _playerPrefab;
	public GameObject _cameraPrefab;
	public GameObject _shieldPrefab;

	#endregion // PUBLIC_MEMBER_VARIABLES



	#region PRIVATE_MEMBER_VARIABLES

	// Transfomations received from players.
	private Transform[] _playerTransforms = new Transform[2];
	private Transform[] _cameraTransforms = new Transform[2];
	private Transform[] _backgroundTransforms = new Transform[2];
	private Transform[] _backgroundAnchorTransforms = new Transform[2];
	private Transform[] _enemyTransforms = new Transform[2];
	private Transform[] _calibrationTransforms = new Transform[2];
	// Time when the values were initialized for each player.
	private float[] _timeInitilized = new float[2];
	// Has been updated player transforms
	private bool[] _updatedPlayerTransform = new bool[2];
	// Check if the scenario has been iniatilized.
	private bool _scenarioInitialized;

	// GameObject Spawned
	private GameObject[] _backgroundGO = new GameObject[2];
	private GameObject[] _playerGO = new GameObject[2];
	private GameObject[] _cameraGO = new GameObject[2];

	// Whether the tracker has been or not detected
	private bool[] _backgroundDetected = new bool[2];
	private bool[] _enemyDetected = new bool[2];

	// Temp GO - Ovoid to create it every update
	GameObject _anchorPoint;
	GameObject _anchorChild;

	// Camera calibrators
	private Vector3[] _calibrationPositionOffset = new Vector3[2];
	private Quaternion[] _calibrationRotationOffset = new Quaternion[2];

	private int counter = 0;

	#endregion // PRIVATE_MEMBER_VARIABLES



	#region GETTERS_AND_SETTERS_METHODS


	#endregion // GETTERS_AND_SETTERS_METHODS



	#region UNTIY_MONOBEHAVIOUR_METHODS

	void Start ()
	{
		_anchorPoint = new GameObject ();
		_anchorChild = new GameObject ();
        
		// Initializing time values.
		_timeInitilized [0] = float.NegativeInfinity;
		_timeInitilized [1] = float.PositiveInfinity;

	}


	void Update ()
	{
		// Initializing scenario
		if (!_scenarioInitialized) {
			// If the set of initialization data from both players has been sent with a small enough delay.
			//if (_timeInitilized [0] > float.NegativeInfinity && _timeInitilized[1] < float.PositiveInfinity) {
			if (Mathf.Abs (_timeInitilized [0] - _timeInitilized [1]) < Globals.Epsilon.initilizeTime && ++counter > 150) {
				InitializeScenario ();
			}
		}
		else {
			// Se podria probar con un &&
			if (_updatedPlayerTransform [0] || _updatedPlayerTransform [1]) {
				UpdateSpawns ();
				_updatedPlayerTransform [0] = false;
				_updatedPlayerTransform [1] = false;
			}
		}
	}

	#endregion // UNTIY_MONOBEHAVIOUR_METHODS



	#region PUBLIC_METHODS

	/** \brief Sends the server the Transform components of the elements we want to track.
	 * 
	 * @param playerId The Id of the player who sends.
	 * @param playerPosition The position of the player who sends.
	 * @param playerRotation The rotation of the player who sends.
	 * @param backgroundPosition The position of the background seen by the player.
	 * @param backgroundRotation The rotation of the background seen by the player.
	 * @param enemyPosition The position of the enemy seen.
	 * @param enemyRotation The rotation of the enemy seen.
	 */
	public void InitilizeTransforms (int playerId, 
	                                 Vector3 cameraPosition, 
	                                 Quaternion cameraRotation, 
	                                 Vector3 backgroundPosition, 
	                                 Quaternion backgroundRotation, 
	                                 Vector3 enemyPosition,
	                                 Quaternion enemyRotation,
	                                 Vector3 calibratorPosition,
	                                 Quaternion calibratorRotation)
	{
		// Saving CAMERA transform
		// To obtain a transform component an object need to be created.
		if (_cameraTransforms [playerId - 1] == null) {
			_cameraTransforms [playerId - 1] = Instantiate (Resources.Load ("Player") as GameObject).transform;
			// We hide it on the hierarchy panel                           
			_cameraTransforms [playerId - 1].gameObject.SetActive (false);
			_cameraTransforms [playerId - 1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		// Set transfrom settings
		_cameraTransforms [playerId - 1].position = cameraPosition;
		_cameraTransforms [playerId - 1].rotation = cameraRotation;

		// Saving PLAYER transform
		// To obtain a transform component an object need to be created.
		if (_playerTransforms [playerId - 1] == null) {
			_playerTransforms [playerId - 1] = Instantiate (Resources.Load ("Player") as GameObject).transform;
			// We hide it on the hierarchy panel                           
			_playerTransforms [playerId - 1].gameObject.SetActive (false);
			_playerTransforms [playerId - 1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		// Set transfrom settings
		_playerTransforms [playerId - 1].position = cameraPosition;
		_playerTransforms [playerId - 1].rotation = cameraRotation;
		_playerTransforms [playerId - 1].transform.Rotate (Vector3.right, 90);
		_playerTransforms [playerId - 1].transform.Rotate (Vector3.up, 180);

		// Saving BACKGROUND transform
		if (_backgroundTransforms [playerId - 1] == null) {
			_backgroundTransforms [playerId - 1] = Instantiate (Resources.Load ("Background") as GameObject).transform;
			_backgroundTransforms [playerId - 1].gameObject.SetActive (false);
			_backgroundTransforms [playerId - 1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		_backgroundTransforms [playerId - 1].position = backgroundPosition;
		_backgroundTransforms [playerId - 1].rotation = backgroundRotation;

		// Saving ENEMY transform
		if (_enemyTransforms [playerId - 1] == null) {
			_enemyTransforms [playerId - 1] = Instantiate (Resources.Load ("Enemy") as GameObject).transform;
			_enemyTransforms [playerId - 1].gameObject.SetActive (false);
			_enemyTransforms [playerId - 1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		_enemyTransforms [playerId - 1].position = enemyPosition;
		_enemyTransforms [playerId - 1].rotation = enemyRotation;

		// Saving CALIBRATION transform
		if (_calibrationTransforms [playerId - 1] == null) {
			_calibrationTransforms [playerId - 1] = Instantiate (Resources.Load ("Calibration") as GameObject).transform;
			_calibrationTransforms [playerId - 1].gameObject.SetActive (false);
			_calibrationTransforms [playerId - 1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		_calibrationTransforms [playerId - 1].position = calibratorPosition;
		_calibrationTransforms [playerId - 1].rotation = calibratorRotation;

		_timeInitilized [playerId - 1] = Time.time;
	}


	/** \brief Update at the server the Transform components of the elements tracked.
	 * 
	 * @param playerId The Id of the player who sends.
	 * @param playerPosition The position of the player who sends.
	 * @param playerRotation The rotation of the player who sends.
	 * @param backgroundDetected Whether the background has been detected or not.
	 * @param backgroundPosition The position of the background seen by the player.
	 * @param backgroundRotation The rotation of the background seen by the player.
	 * @param enemyDetected Whether the enemy has been detected or not.
	 * @param enemyPosition The position of the enemy seen.
	 * @param enemyRotation The rotation of the enemy seen.
	 */
	public void UpdateTransforms (int playerId, 
	                              Vector3 cameraPosition, 
	                              Quaternion cameraRotation, 
	                              bool backgroundDetected,
	                              Vector3 backgroundPosition, 
	                              Quaternion backgroundRotation, 
	                              bool enemyDetected,
	                              Vector3 enemyPosition,
	                              Quaternion enemyRotation)
	{
		// Updating CAMERA transform
		_cameraTransforms [playerId - 1].position = cameraPosition;
		_cameraTransforms [playerId - 1].rotation = cameraRotation;
       

		// Updating PLAYER transform
		_anchorPoint.transform.position = cameraPosition;
		_anchorPoint.transform.rotation = cameraRotation;
		_anchorChild.transform.position = cameraPosition;
		_anchorChild.transform.rotation = cameraRotation;
		_anchorChild.transform.parent = _anchorPoint.transform;
		//Camera tranformation
		_anchorPoint.transform.Rotate (Vector3.right, 90);
		_anchorPoint.transform.Rotate (Vector3.up, 180);
		//Offset transformation
		_anchorChild.transform.localPosition = _calibrationPositionOffset [playerId - 1];
		_anchorChild.transform.localRotation = _calibrationRotationOffset [playerId - 1];

		_playerTransforms [playerId - 1].position = _anchorChild.transform.position;
		_playerTransforms [playerId - 1].rotation = _anchorChild.transform.rotation;

		_anchorChild.transform.parent = null;


		// Updating BACKGROUND transform
		_backgroundDetected [playerId - 1] = backgroundDetected;
		if (backgroundDetected) {
			_backgroundTransforms [playerId - 1].position = backgroundPosition;
			_backgroundTransforms [playerId - 1].rotation = backgroundRotation;
		}

		// Updating ENEMY transform
		_enemyDetected [playerId - 1] = enemyDetected;
		if (enemyDetected) {
			_enemyTransforms [playerId - 1].position = enemyPosition;
			_enemyTransforms [playerId - 1].rotation = enemyRotation;
		}

		_updatedPlayerTransform [playerId - 1] = true;
	}



	#endregion // PUBLIC_METHODS



	#region PRIVATE_METHODS


	private void InitializeScenario ()
	{
		// making a copy to not mofify the originals
		Transform[] playerTransforms = new Transform[2];
		Transform[] backgroundTransforms = new Transform[2];
		Transform[] enemyTransforms = new Transform[2];
		Transform[] calibrationTransforms = new Transform[2];
		playerTransforms [0] = Instantiate (_playerTransforms [0]);
		playerTransforms [1] = Instantiate (_playerTransforms [1]);
		backgroundTransforms [0] = Instantiate (_backgroundTransforms [0]);
		backgroundTransforms [1] = Instantiate (_backgroundTransforms [1]);
		enemyTransforms [0] = Instantiate (_enemyTransforms [0]);
		enemyTransforms [1] = Instantiate (_enemyTransforms [1]);
		calibrationTransforms [0] = Instantiate (_calibrationTransforms [0]);
		calibrationTransforms [1] = Instantiate (_calibrationTransforms [1]);

		// Setting the anchor points
		GameObject anchorPoint0 = new GameObject ();
		GameObject anchorPoint1 = new GameObject ();

		anchorPoint0.transform.position = calibrationTransforms [0].position;
		anchorPoint0.transform.rotation = calibrationTransforms [0].rotation;
		calibrationTransforms [0].parent = anchorPoint0.transform;
		playerTransforms [0].parent = anchorPoint0.transform;
		backgroundTransforms [0].parent = anchorPoint0.transform;
		enemyTransforms [0].parent = anchorPoint0.transform;

		anchorPoint1.transform.position = calibrationTransforms [1].position;
		anchorPoint1.transform.rotation = calibrationTransforms [1].rotation;
		calibrationTransforms [1].parent = anchorPoint1.transform;
		playerTransforms [1].parent = anchorPoint1.transform;
		backgroundTransforms [1].parent = anchorPoint1.transform;
		enemyTransforms [1].parent = anchorPoint1.transform;

		// Matching the calibration points
		anchorPoint0.transform.position = Vector3.zero;
		anchorPoint1.transform.position = Vector3.zero;
		anchorPoint0.transform.rotation = Quaternion.identity;
		anchorPoint1.transform.rotation = Quaternion.identity;
		//-90 first to make them look right
		anchorPoint1.transform.Rotate (new Vector3 (-90, 0, 0));
		anchorPoint0.transform.Rotate (new Vector3 (-90, 0, 0));
		anchorPoint1.transform.Rotate (new Vector3 (0, 0, 180));


		// Saving ANCHOR BACKGROUND transform
		if (_backgroundAnchorTransforms [0] == null) {
			_backgroundAnchorTransforms [0] = new GameObject ("BackgroundAnchor1").transform;
			_backgroundAnchorTransforms [0].gameObject.SetActive (false);
			_backgroundAnchorTransforms [0].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		_backgroundAnchorTransforms [0].position = backgroundTransforms [0].position;
		_backgroundAnchorTransforms [0].rotation = backgroundTransforms [0].rotation;

		if (_backgroundAnchorTransforms [1] == null) {
			_backgroundAnchorTransforms [1] = new GameObject ("BackgroundAnchor2").transform;
			_backgroundAnchorTransforms [1].gameObject.SetActive (false);
			_backgroundAnchorTransforms [1].gameObject.hideFlags = HideFlags.HideInHierarchy;
		}
		_backgroundAnchorTransforms [1].position = backgroundTransforms [1].position;
		_backgroundAnchorTransforms [1].rotation = backgroundTransforms [1].rotation;


		// Spawning background, camera and player references
		for (int iPlayer = 0; iPlayer < 2; iPlayer++) {
			_backgroundGO [iPlayer] = Instantiate (_backgroundPrefab, backgroundTransforms [iPlayer].position, backgroundTransforms [iPlayer].rotation) as GameObject;
			NetworkServer.Spawn (_backgroundGO [iPlayer]);
			_backgroundGO [iPlayer].GetComponent<Spawned> ().RpcSetName ("Background " + (iPlayer + 1));

			_playerGO [iPlayer] = Instantiate (_playerPrefab, playerTransforms [iPlayer].position, playerTransforms [iPlayer].rotation) as GameObject;
			NetworkServer.Spawn (_playerGO [iPlayer]);
			_playerGO [iPlayer].GetComponent<Spawned> ().RpcSetName ("Player " + (iPlayer + 1));

			_cameraGO [iPlayer] = Instantiate (_cameraPrefab, _cameraTransforms [iPlayer].position, _cameraTransforms [iPlayer].rotation) as GameObject;
			NetworkServer.Spawn (_cameraGO [iPlayer]);
			_cameraGO [iPlayer].GetComponent<Spawned> ().RpcSetName ("Camera " + (iPlayer + 1));
		}

		// Saving offsets
		GameObject anchorPoint2 = new GameObject ();
		GameObject anchorPoint3 = new GameObject ();

		anchorPoint2.transform.position = playerTransforms [0].position;
		anchorPoint2.transform.rotation = playerTransforms [0].rotation;
		playerTransforms [0].transform.parent = anchorPoint2.transform;
		enemyTransforms [1].transform.parent = anchorPoint2.transform;

		anchorPoint2.transform.position = Vector3.zero;
		anchorPoint2.transform.rotation = Quaternion.identity;
		_calibrationPositionOffset [0] = enemyTransforms [1].position;
		_calibrationRotationOffset [0] = enemyTransforms [1].localRotation;


		anchorPoint3.transform.position = playerTransforms [1].position;
		anchorPoint3.transform.rotation = playerTransforms [1].rotation;
		playerTransforms [1].transform.parent = anchorPoint3.transform;
		enemyTransforms [0].transform.parent = anchorPoint3.transform;

		anchorPoint3.transform.position = Vector3.zero;
		anchorPoint3.transform.rotation = Quaternion.identity;
		_calibrationPositionOffset [1] = enemyTransforms [0].position;
		_calibrationRotationOffset [1] = enemyTransforms [0].localRotation;


		// Clear all items
		for (int i = 1; i < 2; i++) {
			Destroy (playerTransforms [i].gameObject);
			Destroy (backgroundTransforms [i].gameObject);
			Destroy (enemyTransforms [i].gameObject);
			Destroy (calibrationTransforms [i].gameObject);
		}
		Destroy (anchorPoint0);
		Destroy (anchorPoint1);
		Destroy (anchorPoint2);
		Destroy (anchorPoint3);


		// Inform the rest of actors
		_scenarioInitialized = true;

		foreach (PlayerBehaviour pb in Object.FindObjectsOfType<PlayerBehaviour> ()) {
			pb._initializatedScenario = true;
		}

		Debug.Log ("Scenario Initialized");
	}


	private void UpdateSpawns ()
	{
		bool[] calculablePlayer = new bool[2];
		calculablePlayer [0] = true;
		calculablePlayer [1] = true;

		// If there is not background detected both player are not located
		if (!_backgroundDetected [0] && !_backgroundDetected [1]) {
			// No 1 No 2
			calculablePlayer [0] = false;
			calculablePlayer [1] = false;
			Debug.LogWarning ("Players 1 and 2 are not located");
		}
		// If there is no conection between players and the player can not see the background
		// it is not located
		if (!_backgroundDetected [0] && !_enemyDetected [0] && !_enemyDetected [1]) {
			// No 1
			calculablePlayer [0] = false;
			Debug.LogWarning ("Players 1 is not located");
		}
		if (!_backgroundDetected [1] && !_enemyDetected [0] && !_enemyDetected [1]) {
			// No 2
			calculablePlayer [1] = false;
			Debug.LogWarning ("Players 2 is not located");
		}

	
		for (int iPlayer = 0; iPlayer < 2; iPlayer++) {
			if (calculablePlayer [iPlayer] && _backgroundDetected [iPlayer]) {
				// We set a container to set a custom world coordinate system centered at the Background point
				_anchorPoint.transform.position = _backgroundTransforms [iPlayer].position;
				_anchorPoint.transform.rotation = _backgroundTransforms [iPlayer].rotation;

				// Setting the player transform entities children of the container.
				_backgroundTransforms [iPlayer].parent = _anchorPoint.transform;
				_playerTransforms [iPlayer].parent = _anchorPoint.transform;
				_enemyTransforms [iPlayer].parent = _anchorPoint.transform;
				_cameraTransforms [iPlayer].parent = _anchorPoint.transform;


				// Locating according the anchor Background
				_anchorPoint.transform.position = _backgroundAnchorTransforms [iPlayer].position;
				_anchorPoint.transform.rotation = _backgroundAnchorTransforms [iPlayer].rotation;

				// Setting free hierarchy and destroying temporal objects
				_backgroundTransforms [iPlayer].parent = null;
				_playerTransforms [iPlayer].parent = null;
				_enemyTransforms [iPlayer].parent = null;
				_cameraTransforms [iPlayer].parent = null;


				// Transmiting to GameObject spawned
				_playerGO [iPlayer].transform.position = _playerTransforms [iPlayer].position;
				_playerGO [iPlayer].transform.rotation = _playerTransforms [iPlayer].rotation;
				_cameraGO [iPlayer].transform.position = _cameraTransforms [iPlayer].position;
				_cameraGO [iPlayer].transform.rotation = _cameraTransforms [iPlayer].rotation;
			}
		}

		// At last one player have been located here
		for (int iPlayer = 0; iPlayer < 2; iPlayer++) {
			// If it is calculabre and has no background is because it has seen me or I have seen it
			if (calculablePlayer [iPlayer] && !_backgroundDetected [iPlayer]) {
				if (_enemyDetected [(iPlayer + 1) % 2]) {
					// Setting the container anchor point
					_anchorPoint.transform.position = _playerTransforms [iPlayer].position;
					_anchorPoint.transform.rotation = _playerTransforms [iPlayer].rotation;

					// Setting the player transform entities children of the container.
					_backgroundTransforms [iPlayer].parent = _anchorPoint.transform;
					_playerTransforms [iPlayer].parent = _anchorPoint.transform;
					_enemyTransforms [iPlayer].parent = _anchorPoint.transform;
					_cameraTransforms [iPlayer].parent = _anchorPoint.transform;

					// Locating according the Enemy
					_anchorPoint.transform.position = _enemyTransforms [(iPlayer + 1) % 2].position;
					_anchorPoint.transform.rotation = _enemyTransforms [(iPlayer + 1) % 2].rotation;

					// Setting free hierarchy and destroying temporal objects
					_backgroundTransforms [iPlayer].parent = null;
					_playerTransforms [iPlayer].parent = null;
					_enemyTransforms [iPlayer].parent = null;
					_cameraTransforms [iPlayer].parent = null;
				}

				if (_enemyDetected [iPlayer]) {
					// Setting the container anchor point
					_anchorPoint.transform.position = _enemyTransforms [iPlayer].position;
					_anchorPoint.transform.rotation = _enemyTransforms [iPlayer].rotation;

					// Setting the player transform entities children of the container.
					_playerTransforms [iPlayer].parent = _anchorPoint.transform;
					_enemyTransforms [iPlayer].parent = _anchorPoint.transform;
					_backgroundTransforms [iPlayer].parent = _anchorPoint.transform;
					_cameraTransforms [iPlayer].parent = _anchorPoint.transform;

					// Locating according the Enemy
					_anchorPoint.transform.position = _playerTransforms [(iPlayer + 1) % 2].position;
					_anchorPoint.transform.rotation = _playerTransforms [(iPlayer + 1) % 2].rotation;

					// Setting free hierarchy and destroying temporal objects
					_backgroundTransforms [iPlayer].parent = null;
					_playerTransforms [iPlayer].parent = null;
					_enemyTransforms [iPlayer].parent = null;
					_cameraTransforms [iPlayer].parent = null;
				}

				// Transmiting to GameObject spawned
				_playerGO [iPlayer].transform.position = _playerTransforms [iPlayer].position;
				_playerGO [iPlayer].transform.rotation = _playerTransforms [iPlayer].rotation;
				_cameraGO [iPlayer].transform.position = _cameraTransforms [iPlayer].position;
				_cameraGO [iPlayer].transform.rotation = _cameraTransforms [iPlayer].rotation;
			}
		}
	}

	#endregion // PRIVATE_METHODS
}
