﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Vuforia;
using System.Collections.Generic;

public class PlayerBehaviour : NetworkBehaviour
{
	#region PUBLIC_MEMBER_VARIABLES

	// Player ID
	[SyncVar]
	public int _playerId;

	//! Check weather the server has initializated the scenario
	[SyncVar]
	public bool _initializatedScenario;
	public bool _lastInitializatedScenario;

	public int _sendsPerSecond = 29;

	// Targets ID
	//! ID's of background targets
	public string[] backgroundTargetIds;
	//! ID's of player targets
	public string[] enemyTargetIds;
	//! ID's of player targets
	public string[] calibratorTargetIds;

	public GameObject bulletPrefab;
	//public Transform bulletSpawn;

	public float speed = 6.0f;

	private float _timeToShoot;
	public float _shootDelay = 2f;


	#endregion // PUBLIC_MEMBER_VARIABLES



	#region PRIVATE_MEMBER_VARIABLES

	private LocationBehaviour _locationBehaviour;

	private float _lastTime;

	// Trackable saved
	private TrackableBehaviour backgroundTrackable;
	private TrackableBehaviour enemyTrackable;
	private TrackableBehaviour calibratorTrackable;

	private GameObject _cameraPlayer;
	private GameObject _playerGORef;

	GameObject arCamera;
	GameObject anchorPoint;

	#endregion // PRIVATE_MEMBER_VARIABLES



	#region GETTERS_AND_SETTERS_METHODS


	#endregion // GETTERS_AND_SETTERS_METHODS



	#region UNTIY_MONOBEHAVIOUR_METHODS

	void Start ()
	{
		InitializeTrackables ();
       
		_locationBehaviour = Object.FindObjectOfType<LocationBehaviour> ();
		arCamera = GameObject.Find ("ARCamera");
		anchorPoint = new GameObject ();
	}

	void Update ()
	{
		if (!isLocalPlayer) {
			return;
		}

		if (!_initializatedScenario) {
			InitializeScenario ();
		}
		else if (!_lastInitializatedScenario) {
			if ((_cameraPlayer = GameObject.Find ("Camera " + _playerId)) != null &&
			    (_playerGORef = GameObject.Find ("Player " + _playerId)) != null)
				_lastInitializatedScenario = true;
		}
		else {
			UpdateScenario ();
		}

		if (Input.touchCount > 0 || Input.GetKeyDown (KeyCode.Space)) {
			CmdFire (_playerGORef.transform.position, 
				_playerGORef.transform.up);
		}
	}


	#endregion /			/ UNTIY_MONOBEHAVIOUR_METHODS



	#region PUBLIC_METHODS



	#endregion // PUBLIC_METHODS



	#region PRIVATE_METHODS

	/** \brief Inializate Trackers
	 */
	private void InitializeTrackables ()
	{

		// Get the StateManager
		StateManager sm = TrackerManager.Instance.GetStateManager ();

		// Query the StateManager to retrieve the list of
		// currently 'active' trackables  
		//(i.e. the ones currently being tracked by Vuforia)
		IEnumerable<TrackableBehaviour> trackables = sm.GetTrackableBehaviours ();

		foreach (TrackableBehaviour tb in trackables) {
			if (tb.TrackableName == backgroundTargetIds [_playerId - 1]) {
				backgroundTrackable = tb;
			}
			else if (tb.TrackableName == enemyTargetIds [_playerId - 1]) {
				enemyTrackable = tb;
			}
			else if (tb.TrackableName == calibratorTargetIds [_playerId - 1]) {
				calibratorTrackable = tb;
			}
		}
	}

	/** \brief Inializate Scenario
	 */
	private void InitializeScenario ()
	{
		
		if (backgroundTrackable.CurrentStatus == TrackableBehaviour.Status.TRACKED
		    && enemyTrackable.CurrentStatus == TrackableBehaviour.Status.TRACKED
		    && calibratorTrackable.CurrentStatus == TrackableBehaviour.Status.TRACKED) {

			CmdInitilizeScenarioServer (_playerId,
				Camera.main.transform.position,
				Camera.main.transform.rotation, 
				backgroundTrackable.transform.position,
				backgroundTrackable.transform.rotation,
				enemyTrackable.transform.position,
				enemyTrackable.transform.rotation,
				calibratorTrackable.transform.position,
				calibratorTrackable.transform.rotation);
		}
	}

	/** \brief Sends the server the Transform components of the elements we want to track.
	 * 
	 * @param playerId The Id of the player who sends.
	 * @param playerPosition The position of the player who sends.
	 * @param playerRotation The rotation of the player who sends.
	 * @param backgroundPosition The position of the background seen by the player.
	 * @param backgroundRotation The rotation of the background seen by the player.
	 * @param enemyPosition The position of the enemy seen.
	 * @param enemyRotation The rotation of the enemy seen.
	 */
	[Command]
	private void CmdInitilizeScenarioServer (int playerId, 
	                                         Vector3 playerPosition, 
	                                         Quaternion playerRotation, 
	                                         Vector3 backgroundPosition, 
	                                         Quaternion backgroundRotation, 
	                                         Vector3 enemyPosition,
	                                         Quaternion enemyRotation,
	                                         Vector3 calibratorPosition,
	                                         Quaternion calibratorRotation)
	{
		_locationBehaviour.InitilizeTransforms (_playerId,
			playerPosition, 
			playerRotation, 
			backgroundPosition, 
			backgroundRotation, 
			enemyPosition,
			enemyRotation,
			calibratorPosition,
			calibratorRotation);
	}


	private void UpdateScenario ()
	{
		CmdUpdateScenarioServer (_playerId,
			Camera.main.transform.position,
			Camera.main.transform.rotation,
			backgroundTrackable.CurrentStatus == TrackableBehaviour.Status.TRACKED,
			backgroundTrackable.transform.position,
			backgroundTrackable.transform.rotation,
			enemyTrackable.CurrentStatus == TrackableBehaviour.Status.TRACKED,
			enemyTrackable.transform.position,
			enemyTrackable.transform.rotation);

		// Update camera tracking position
		// I have to carry all the trackables because they don't update position everyframe,
		// and the need to be stable for the next update
		anchorPoint.transform.position = arCamera.transform.position;
		anchorPoint.transform.rotation = arCamera.transform.rotation;

		backgroundTrackable.transform.parent = anchorPoint.transform;
		enemyTrackable.transform.parent = anchorPoint.transform;
		arCamera.transform.parent = anchorPoint.transform;

		anchorPoint.transform.position = _cameraPlayer.transform.position;
		anchorPoint.transform.rotation = _cameraPlayer.transform.rotation;

		backgroundTrackable.transform.parent = null;
		enemyTrackable.transform.parent = null;
		arCamera.transform.parent = null;

	}


	/** \brief Update at the server the Transform components of the elements tracked.
	 * 
	 * @param playerId The Id of the player who sends.
	 * @param playerPosition The position of the player who sends.
	 * @param playerRotation The rotation of the player who sends.
	 * @param backgroundDetected Whether the background has been detected or not.
	 * @param backgroundPosition The position of the background seen by the player.
	 * @param backgroundRotation The rotation of the background seen by the player.
	 * @param enemyDetected Whether the enemy has been detected or not.
	 * @param enemyPosition The position of the enemy seen.
	 * @param enemyRotation The rotation of the enemy seen.
	 */
	[Command]
	public void CmdUpdateScenarioServer (int playerId, 
	                                     Vector3 playerPosition, 
	                                     Quaternion playerRotation, 
	                                     bool backgroundDetected,
	                                     Vector3 backgroundPosition, 
	                                     Quaternion backgroundRotation, 
	                                     bool enemyDetected,
	                                     Vector3 enemyPosition,
	                                     Quaternion enemyRotation)
	{
		_locationBehaviour.UpdateTransforms (_playerId,
			playerPosition, 
			playerRotation, 
			backgroundDetected,
			backgroundPosition, 
			backgroundRotation, 
			enemyDetected,
			enemyPosition,
			enemyRotation);
	}


	// This [Command] code is called on the Client …
	// … but it is run on the Server!
	[Command]
	void CmdFire (Vector3 position, Vector3 direction)
	{
		if (_timeToShoot < Time.time) {
			// Create the Bullet from the Bullet Prefab
			var bullet = (GameObject)Instantiate (
				             bulletPrefab,
				             position + direction.normalized,
				             Quaternion.identity);
			//bulletSpawn.position,
			//bulletSpawn.rotation);

			// Add velocity to the bullet
			bullet.GetComponent<Rigidbody> ().velocity = direction * speed;

			// Spawn the bullet on the Clients
			NetworkServer.Spawn (bullet);

			// Destroy the bullet after 2 seconds
			Destroy (bullet, 2.0f);

			_timeToShoot = Time.time + _shootDelay;
		}
	}

	#endregion // PRIVATE_METHODS
}
