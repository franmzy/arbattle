﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Spawned : NetworkBehaviour
{
	[ClientRpc]
	public void RpcSetName (string name)
	{
		this.name = name;
	}
}
