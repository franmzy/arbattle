﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MyNetworkManager : NetworkManager
{
	private int _playerIdCounter = 1;

	// called when a new player is added for a client
	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId)
	{
		GameObject player = (GameObject)Instantiate (playerPrefab, Vector3.zero, Quaternion.identity);
		player.GetComponent<PlayerBehaviour> ()._playerId = _playerIdCounter++;
		NetworkServer.AddPlayerForConnection (conn, player, playerControllerId);
	}
}
